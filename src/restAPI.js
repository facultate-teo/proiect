import validate from "express-validation"
import validation from "./validation.js";
import service from "./service.js";
import store from "./store.js";
import * as mongodb from './mongodb.js';
console.log(mongodb)
// console.log(mstore)
module.exports = (router) => {
    let path = "/testing"


    //ARRAY INJECTIONS
    router.post(`${path}/arrayinjection`, (req, res) => {
        let username = req.body.username;
        let password = req.body.password;
        
        store.getUser(username, password)
            .then( (result) => {
                console.log('get user', result);
                res.status(200).send(result);
            })
            .catch( (err) => {
                res.status(500).send(err);
            });
    });

    router.post(`${path}/arrayinjection/save`, (req, res) => {
        let username = req.body.username;
        let password = req.body.password;
        
        store.saveUser(username, password)
            .then( (result) => {
                console.log('save user', result);
                res.status(200).send(result);
            })
            .catch( (err) => {
                res.status(500).send(err);
            });
    });

    //OR INJECTION
    router.post(`${path}/orinjection`, (req, res) => {
        let username = req.body.username;
        let password = req.body.password;
        
        store.getUserORInjection(username, password)
            .then( (result) => {
                console.log('get user', result);
                res.status(200).send(result);
            })
            .catch( (err) => {
                res.status(500).send(err);
            });
    });
    //JAVASCRIPT INJECTION
    router.post(`${path}/jsinjection`, (req, res) => {
        // let param = req.body.param;
        let map = req.body.map
        let reduce = req.body.reduce
        // eval(param)
        console.log(map)
        console.log(reduce)
        
        const mapF = `function() {emit(this.cust_id, this.${map});}`
        console.log(mapF.length)
        console.log(mapF.substring(86))
        const reduceF = `function(key,values) {${reduce}}`

        const opts ={
            out: "order_total", 
        }
        console.log(mapF)
        //mongodb.mapReduce(mapF,reduceF,opts)
        mongodb.mapReduce(map)
            .then( (result) => {
                console.log('post mapReduce', result);
                res.status(200).send(result);
            })
            .catch( (err) => {
                res.status(500).send(err);
            });
    });

    router.post(`${path}/jsinjection/save`, (req, res) => {
        
        mongodb.save(req.body)
            .then( (result) => {
                console.log('save user', result);
                res.status(200).send(result);
            })
            .catch( (err) => {
                res.status(500).send(err);
            });
    });



    // router.get(`${path}/:id`, validate(validation.get), (req, res) => {
    //     service.getItem(req.params.id)
    //         .then( (result) => {
    //             console.log('get message', result);
    //             res.status(200).send(result);
    //         })
    //         .catch( (err) => {
    //             res.status(500).send(err);
    //         });
    // });

    // router.put(`${path}/:id`, validate(validation.put), (req, res) => {
    //     service.updateItem(req.body,req.params.id)
    //         .then( (result) => {
    //             console.log('update message', result);
    //             res.status(200).send(result);
    //         })
    //         .catch( (err) => {
    //             res.status(500).send(err);
    //         });
        
    // });



    // router.delete(`${path}/:id`, validate(validation.delete), (req, res) => {
    //     service.deleteItem(req.params.id)
    //         .then( (result) => {
    //             console.log('delete message', result);
    //             res.status(200).send(result);
    //         })
    //         .catch( (err) => {
    //             res.status(500).send(err);
    //         });
    // });

};
