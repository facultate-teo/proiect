import dotenv from 'dotenv'
dotenv.load();
import express from "express";
let app = express();
import bodyParser from "body-parser";
import restAPI from "./restAPI.js";

app.use(bodyParser.urlencoded(
    {extended : true}
));
app.use(bodyParser.json());

let port = process.env.PORT;
let router = express.Router();
restAPI(router);
app.use( '/api', router );

app.listen(port, '0.0.0.0');

console.log( 'server is running on port ' + port );
