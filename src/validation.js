import Joi from "joi";

const id =  Joi.string().length(24).required()

const phone_regex = /(\+?[0-9]{1,2}|\(\+?[0-9]{1,2}\))?[.-\s]?(\([0-9]{3}\)|[0-9]{3})[.-\s]?[0-9]{3}[.-\s]?[0-9]{3}/

let person = {
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().trim().regex(phone_regex).required(),
}

let clientFields = {
    ...person,
    description: Joi.string().required(),
    image: Joi.string().uri({scheme: ['http', 'https']}).required(),
    type: Joi.string().required(),
    location: {
        lat: Joi.number().required(),
        long: Joi.number().required(),
    }
}

let report = {
    ...clientFields,
    status: Joi.string().required(),
    createTime: Joi.date().timestamp().raw().required(), 
    people: Joi.array().items(person).min(0),
    solvedBy: person,
    solvedTime: Joi.date().timestamp().raw(),
}

module.exports = {
    getItems: {
        query: {
            page: Joi.number().integer().positive(),
            limit: Joi.number().integer().min(0),
        }
    },
    post: {
        body: clientFields,
    },
   get: {
       params: {
           id, 
       }
   },
   put: {
       params: {
           id, 
       },
       body: report,
   },
   delete: {
       params: {
           id, 
       }
   } 
}
