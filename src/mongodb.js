// Mongo
import mongodb from 'mongodb';
console.log("aaaaaaa")
var server = new mongodb.Server('localhost', 27017, {auto_reconnect: true});
var db = new mongodb.Db('mydb', server, { w: 1 });
db.open(function() {});
import q from "q";
let collection;
db.collection('users', function(error, users) {
    collection = users;
});
export const a =  { v: "bla"}
export const save = (obj) => {
    let def = q.defer();
    collection.insert({
        cust_id: obj.cust_id, 
        amount: obj.amount,
        status: 'A'
    }, (err,obj) => {
        if(err) def.reject(err)
        def.resolve(obj);

    });
    return def.promise;
}
/*
export const mapReduce = (map, reduce, opts) => {
    let def = q.defer();
    collection.mapReduce(map, reduce, opts,(err,obj) => {
        if(err) def.reject(err)
        obj.find({}).toArray((err, results) =>{
            if(err) def.reject(err)
            def.resolve(results);    
        })
        
    })

    return def.promise
}*/
export const mapReduce = (param) => {
    let def = q.defer();
    let map = `function() { emit(this.cust_id, this.${param}); }`; 
    let reduce = "function(name, sum) { return Array.sum(sum); }"; 
    let opt = "{ out: 'totals' }";
    let final = `db.users.mapReduce(${map}, ${reduce}, ${opt});`
    console.log(final)
    db.eval(final,[], {nolock:true}, (err, result) => {
        if(err) def.reject(err)
        def.resolve(result); 
    })
    /*
    collection.mapReduce(map, reduce, opts,(err,obj) => {
        if(err) def.reject(err)
        obj.find({}).toArray((err, results) =>{
            if(err) def.reject(err)
            def.resolve(results);    
        })
        
    })
    */
    return def.promise
}

// export function getUser(id: string, callback: (user: User) => void) {
//     db.collection('users', function(error, users) {
//         if(error) { console.error(error); return; }
//         users.find({_id: id}).batchSize(10).nextObject(function(error, user) {
//             if(error) { console.error(error); return; }
//             callback(user);
//         });
//     });
// }

// export function getUsers(callback: (users: User[]) => void) {
//     db.collection('users', function(error, users_collection) {
//         if(error) { console.error(error); return; }
//         users_collection.find({}, { '_id': 1 }).toArray(function(error, userobjs) {
//            if(error) { console.error(error); return; }
//            callback(userobjs);
//         });
//     });
// }

// export function getImage(imageId: string, callback: (image: Image) => void) {
//     db.collection('images', function(error, images_collection) {
//         if(error) { console.error(error); return; }
//         images_collection.find({_id: new mongodb.ObjectID(imageId)}).batchSize(10).nextObject(function(error, image) {
//             if(error) { console.error(error); return; }
//             callback(image);
//         });
//     });
// }

// export function getImages(imageIds: mongodb.ObjectID[], callback: (images: Image[]) => void) {
//     db.collection('images', function(error, images_collection) {
//         if(error) { console.error(error); return; }
//         images_collection.find({_id: {$in: imageIds}}).toArray(function(error, images) {
//             callback(images);
//         });
//     }); 
// }

// export function addBoard(userid: any, title: string, description: string, callback: (user: User) => void) {
//     db.collection('users', function(error, users) {
//         if(error) { console.error(error); return; }
//         users.update(
//             {_id: userid}, 
//             {"$push": {boards: { title: title, description: description, images: []}}}, 
//             function(error, user) {
//                 if(error) { console.error(error); return; }
//                 callback(user);
//             }
//         );
//     });
// }

// export function addPin(userid: string, boardid: string, imageUri: string, link: string, caption: string, callback: (user: User) => void) {
//     db.collection('images', function(error, images_collection) {
//         if(error) { console.error(error); return; }
//         images_collection.insert({
//             user: userid,
//             caption: caption,
//             imageUri: imageUri,
//             link: link,
//             board: boardid,
//             comments: []
//         }, function(error, image) {
//             console.log(image);
//             db.collection('users', function(error, users) {
//                 if(error) { console.error(error); return; }
//                 users.update(
//                     {_id: userid, "boards.title": boardid}, 
//                     {"$push": {"boards.$.images": image[0]._id}},
//                     function(error, user) {
//                         callback(user);
//                     }
//                 );
//             })
//         })
//     })
// }