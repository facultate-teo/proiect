import config from "./config.js";
import Promise from 'bluebird';
import mongo from "monk";
import q from "q";
import {ObjectID} from 'mongodb';

const getTime =  () => {
    return (new Date()).getTime();
};


class Store {

    connect (databaseUrl, collection) {
        console.log(databaseUrl,collection)
        this.collectionName = collection;
        this.databaseUrl = databaseUrl;
        this.db = mongo(databaseUrl);

        this.collection = this.db.get(collection);
        
        console.log(`connected to ${collection} store`);
    }

    getUser(username, password){
        let that = this;
        let def = q.defer();
        that.collection.find({
             username: username,
             password: password
        }).then((user) => {
                def.resolve(user)
            });

        return def.promise;
    }
    getUserORInjection(username, password){
        let that = this;
        let def = q.defer();
        let query = `({ username: '${username}', password: '${password}' })`//, password: ${password} }`
        console.log(query)
        let params = eval(query)
        console.log("params", params)
        //{ username: 'pavel', $or: [ {}, { a: 'a', password: '' } ], username: 'pavel'}
        //{ username: 'pavel', '$or': [ {}, { a: 'a', password: '' } ], '$comment': 'pavel'}
        that.collection.find(params).then((user) => {
                def.resolve(user)
            });

        return def.promise;
    }
    saveUser (username, password) {
        let that = this;
        
        return that.collection.insert({
                username: username,
                password: password,
            });
    }

    // testDB(username, password){
    //     let that = this;
    //     let def = q.defer();
    //     console.log("this.db", this.db)
    //     this.db.collections[this.collectionName].find({
    //          username: username,
    //          password: password
    //     }).then((user) => {
    //             def.resolve(user)
    //         });

    //     return def.promise;
    // }
    getMapReduce(username, password){
        let that = this;
        let def = q.defer();
        
        that.collection.group(
            { password: true },
            {},
            { count: 0 },
            function (obj, prev) {
                prev.count++
            },
            true
            )
            .then((user) => {
                def.resolve(user)
            });

        return def.promise;
    }
    
}
let store = new Store();
store.connect(config.connectionUrl, config.databaseName);

module.exports = store;
