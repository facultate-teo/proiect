import dotenv from 'dotenv';
dotenv.load();
let config = {
    connectionUrl : process.env.MONGO_URL,
    databaseName : 'NoSqlInjection',
    mongoose: 'Orders'
};

module.exports = config;