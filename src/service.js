import store from "./store.js";
import q from "q";

let mapper = (item) => {
	item.id = item._id;
	delete item._id;
	
	return item;
}

let unmapper = (item) => {
	item._id = item.id;
	delete item.id;
	
	return item;
}

export const authentification = (username, password) => {
	let def = q.defer();
	
	store.getUser(page, limit)
		.then((items) => {
			def.resolve(items.map(mapper));
		});
	return def.promise;
}
	


// let getItems = (page, limit) => {
// 	let def = q.defer();
	
// 	store.getItems(page, limit)
// 		.then((items) => {
// 			def.resolve(items.map(mapper));
// 		});
	
// 	return def.promise;
// }

// let getItem = (id) => {
// 	let def = q.defer();
	
// 	store.getItem(id)
// 		.then((item) => {
// 			def.resolve(mapper(item));
// 		});
	
// 	return def.promise;
// }

// let saveItem = (item) => {
// 	let def = q.defer();
	
// 	store.saveItem(item)
// 		.then((item) => {
// 			def.resolve(mapper(item));
// 		});

// 	return def.promise;

// }

// let updateItem = (item, id) => {
// 	let def = q.defer();
	
// 	item.id = id;
// 	store.updateItem(unmapper(item))
// 		.then((item) => {
// 			def.resolve(mapper(item));
// 		});
	
// 	return def.promise;
// }

// let deleteItem = (id) => {
// 	let def = q.defer();

// 	store.deleteItem(id)
// 		.then((item) => {
// 			def.resolve(mapper(item));
// 		});
	
// 	return def.promise;
// }

// module.exports = {
// 	getItems,
// 	getItem,
// 	saveItem,
// 	updateItem,
// 	deleteItem,
// }